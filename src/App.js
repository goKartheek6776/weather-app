import React, { Component } from 'react';
import logo from './logo.svg';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { LocationList } from './LocationList.js';

import './App.css';

class App extends Component {

  render() {
    const locationDetails = [
    {
      place: 'Nellore',
      weather: 29,
      humidity: 58
    },
    {
      place: 'Ooty',
      weather: 29,
      humidity: 58
    },{
      place: 'Machilipatnam',
      weather: 29,
      humidity: 58
    },
    {
      place: 'Nellore',
      weather: 29,
      humidity: 58
    },
    {
      place: 'Ooty',
      weather: 29,
      humidity: 58
    },{
      place: 'Machilipatnam',
      weather: 29,
      humidity: 58
    },
    {
      place: 'Vijayawada',
      weather: 29,
      humidity: 58
    }
    ];

    return (
      <div className="App">
        <MuiThemeProvider>
          <LocationList data={locationDetails}></LocationList>
        </MuiThemeProvider>
      </div>
    );
  }
}

export default App;
