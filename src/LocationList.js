import * as React from 'react';
import { ListTile } from './ListTile';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import FontIcon from 'material-ui/FontIcon';


import './LocationList.css'

export class LocationList extends React.Component {
	constructor(props) {
		super(props);
	}

	render() {
		const styles = {
		  button: {
		    margin: 12,
		  },
		  exampleImageInput: {
		    cursor: 'pointer',
		    position: 'absolute',
		    top: 0,
		    bottom: 0,
		    right: 0,
		    left: 0,
		    width: '100%',
		    opacity: 0,
		  },
		};
		const list = this.props.data.map(location => {
			return (
				<ListTile location={location}></ListTile>
			);
		});

		return (
			<div>
				<AppBar title="Onsen Weather"></AppBar>
				<div>
					{list}
				</div>
				<RaisedButton
				      label="Add Location"
				      style={styles.button}
				      icon={<FontIcon className="muidocs-icon-custom-github" />}
				    />
			</div>
		);
	}
	


}